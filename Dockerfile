# To build and run with Docker:
#
#  $ docker build -t vdc-front .
#  $ docker run -it --rm -p 3000:3000 -p 3001:3001 --name vdc-front vdc-front
#
FROM node:latest

RUN mkdir -p /vdc-front /home/nodejs && \
    groupadd -r nodejs && \
    useradd -r -g nodejs -d /home/nodejs -s /sbin/nologin nodejs && \
    chown -R nodejs:nodejs /home/nodejs

WORKDIR /vdc-front
COPY package.json typings.json /quickstart/
RUN npm install --unsafe-perm=true

COPY . /vdc-front
RUN chown -R nodejs:nodejs /vdc-front
USER nodejs

CMD npm start
