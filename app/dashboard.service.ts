/* 
 * Copyright (C) 2016 VDC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Injectable }    from '@angular/core';

import { Category } from './category';
import { Loop } from './loop';
import { Runner } from './runner';
import { LoopByRunner } from './loop-by-runner';

@Injectable()
export class DashboardService {
  constructor() 
  {}

  filterScratch(categories: Category[]): Promise<LoopByRunner[]> {
		console.log('Filtering loops for scratch.');

		var scratch: LoopByRunner[] = [];
		for(var category in categories){
			var loopsByRunner = categories[category].loopsByRunner;
			var categoryName = categories[category].category;
			console.log("categoryName" + categoryName);
			if(loopsByRunner.length > 0 && categoryName != "MIXTE"  && categoryName != "JEUNE"  && categoryName != "HOMME" && categoryName != "FEMME"){
				scratch.push.apply(scratch, loopsByRunner);
			}
		}

		// Sort based on runner speed...
		scratch.sort((a, b) => {
			if(a.runner.speed < b.runner.speed){
				return 1;
			}

			if(a.runner.speed > b.runner.speed){
				return -1;
			}

			return 0;
		});

		return Promise.resolve(scratch);
	}

  filterWomen(scratch: LoopByRunner[]): Promise<LoopByRunner[]> {
		console.log('Filtering loops for women.');

		var women: LoopByRunner[] = [];
		for(var loopByRunner in scratch){
			if(scratch[loopByRunner].runner.isWoman){
				women.push(scratch[loopByRunner]);
			}
		}

		return Promise.resolve(women);
	}

  filterMen(scratch: LoopByRunner[]): Promise<LoopByRunner[]> {
		console.log('Filtering loops for men.');

		var men: LoopByRunner[] = [];
		for(var loopByRunner in scratch){
			if(!scratch[loopByRunner].runner.isWoman){
				men.push(scratch[loopByRunner]);
			}
		}

		return Promise.resolve(men);
	}
}