/* 
 * Copyright (C) 2016 VDC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Component } from '@angular/core';

import { Run } from './run';

// import { MomentModule }    from 'angular2-moment';
import * as moment from 'moment';

@Component({
  selector: 'vdc',
  templateUrl: 'app/app.template.html'
})

export class AppComponent{
	// TODO Use the one from Run injected via a constructor
  run: Run = {
		title: 'Les Virades de l\'Espoir',
		date: '25 Septembre 2016',
		location: 'Toulouse',
		length: 954,
		start: new Date(),
		end: null
  };

  constructor(){
  	moment.locale('fr-fr');	
  }
}