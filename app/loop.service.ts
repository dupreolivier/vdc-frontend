/* 
 * Copyright (C) 2016 VDC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Runner } from './runner';
import { Team } from './team';
import { Category } from './category';

import { BASE_URL } from './constants';

@Injectable()
export class LoopService {
	private loopsUrl = BASE_URL + '/loops';
	private headers = new Headers({
		'Content-Type': 'application/json',
		'Access-Control-Allow-Origin': 'localhost:3000'
	});

  constructor(private http: Http) 
  {}

  addLoop(bib: string): Promise<void> {
		console.log('Adding loop for bib: ' + bib);

  	const url = `${this.loopsUrl}/${bib}`;
  	console.log('Calling URL: ' + url);

   	return this.http.post(url, JSON.stringify(""), {headers: this.headers})
						.toPromise()
						.then(() => Promise.resolve(null))
						.catch(this.handleError);
	}
	
  getAllLoopsByCategories(): Promise<Category[]> {
		console.log('Getting all loops by runner, by category.');

  	const url = this.loopsUrl + '/categories';
  	console.log('Calling URL: ' + url);

   	return this.http.get(url, {headers: this.headers})
						.toPromise()
						.then(response => Promise.resolve(response.json() as Category[]))
						.catch(this.handleError);
	}

	private handleError(error: any): Promise<any> {
	  console.error('An error occurred', error); // for demo purposes only
	  return Promise.reject(error.message || error);
	}
}