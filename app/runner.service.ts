/* 
 * Copyright (C) 2016 VDC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Injectable }    from '@angular/core';
import { Headers, Http, RequestOptions, RequestOptionsArgs } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Runner } from './runner';

import { BASE_URL } from './constants';


@Injectable()
export class RunnerService {
	runnersUrl = BASE_URL + '/runners';
	private headers = new Headers({
		'Content-Type': 'application/json',
		'Access-Control-Allow-Origin': 'localhost:3000'
	});

  constructor(private http: Http) { }

  getRunners(): Promise<Runner[]> {
		console.log('Getting all runners;');
  	console.log('Calling URL: ' + this.runnersUrl);

   	return this.http.get(this.runnersUrl)
						.toPromise()
						.then(response => Promise.resolve(response.json() as Runner[]))
						.catch(this.handleError);
	}


	getRunner(bib: string): Promise<Runner> {
		console.log('Getting runner: ' + bib);

  	const url = `${this.runnersUrl}/${bib}`;
  	console.log('Calling URL: ' + url);

   	return this.http.get(url)
						.toPromise()
						.then(response => Promise.resolve(response.json() as Runner))
						.catch(this.handleError);
	  // return this.getRunners()
	  // 				.then(allRunners => Promise.resolve(allRunners.find(runner => runner.bib === bib)));
	}

	update(runner: Runner): Promise<Runner> {
		return this.http.put(this.runnersUrl, JSON.stringify(runner), {headers: this.headers})
	    .toPromise()
	    .then(() => Promise.resolve(runner))
	    .catch(this.handleError);
	}

	create(bib: string, firstName: string, lastName: string, email: string): Promise<Runner> {
		var runner = {firstName: firstName, lastName: lastName, email: email, bib: bib};

		console.log('Creating runner : ' + runner);

		return this.http.post(this.runnersUrl, JSON.stringify(runner), {headers: this.headers})
						.toPromise()
						.then(response => Promise.resolve(response.json() as Runner))
						.catch(this.handleError);
	}

	add(csv: string): Promise<Runner[]> {
		console.log('Adding list of runners from CSV.');
  	const url = `${this.runnersUrl}/add`;
  	
		return this.populateRunners(csv, url);
	}

	overwrite(csv: string): Promise<Runner[]> {
		console.log('Overwriting with a list of runners from CSV.');
  	const url = `${this.runnersUrl}/overwrite`;

		return this.populateRunners(csv, url);
	}

	private populateRunners(csv: string, url: string): Promise<Runner[]>{
		csv = csv.replace(/\n/g, '$$$$');
  	console.log('Calling URL: ' + url);

		return this.http.post(url, JSON.stringify(csv), {headers: this.headers})
						.toPromise()
						.then(response => Promise.resolve(response.json() as Runner[]))
						.catch(this.handleError);
	}

	delete(runner: Runner): Promise<any> {
		console.log('Deleting runner: ' + runner);

  	const url = `${this.runnersUrl}/${runner.bib}`;
  	console.log('Calling URL: ' + url);

		return this.http.delete(url, {headers: this.headers})
				    .toPromise()
				    .then(() => Promise.resolve(true))
				    .catch(this.handleError);
	}

	private handleError(error: any): Promise<any> {
	  console.error('An error occurred', error); // for demo purposes only
	  return Promise.reject(error.message || error);
	}
}