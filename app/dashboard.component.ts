/* 
 * Copyright (C) 2016 VDC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoopService } from './loop.service';
import { RunService } from './run.service';
import { DashboardService } from './dashboard.service';

import { Category } from './category';
import { Loop } from './loop';
import { Runner } from './runner';

import { LoopByRunner } from './loop-by-runner';


@Component({
  selector: 'dashboard',
  templateUrl: 'app/dashboard.template.html',
  styleUrls: ['app/dashboard.component.css']
})

export class DashboardComponent implements OnInit {
	displayAllLoops: boolean = false;
	categories: Category[] = [];
	myDate: Date;
	scratch: LoopByRunner[] = [];
	women: LoopByRunner[] = [];
	men: LoopByRunner[] = [];

	constructor(
		private loopService : LoopService,
		private dashboardService: DashboardService,
		private router: Router)
	{
		this.myDate = new Date();
	}

  ngOnInit(): void {
  	this.retrieveLoopsByCategories();
  }

	retrieveLoopsByCategories(): void {
		this.loopService.getAllLoopsByCategories()
				.then(returnedCategories => {
					this.categories = returnedCategories;
					this.dashboardService.filterScratch(this.categories)
						.then(scratch => {
							this.scratch = scratch;
							this.dashboardService.filterWomen(this.scratch)
								.then(women => this.women = women)
								.catch(this.handleError);
							this.dashboardService.filterMen(this.scratch)
								.then(men => this.men = men)
								.catch(this.handleError);
						})
						.catch(this.handleError);
				}).catch(this.handleError);
	}

	private handleError(error: any): Promise<any> {
	  console.error('An error occurred', error); // for demo purposes only
	  return Promise.reject(error.message || error);
	};

	gotoDetail(runner: Runner): void { 
	  let link = ['/runners', runner.bib];
	  this.router.navigate(link);
	}
}