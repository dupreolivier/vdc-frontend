/* 
 * Copyright (C) 2016 VDC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Team } from './team';
import { TeamService } from './team.service';

@Component({
  selector: 'teams',
  templateUrl: 'app/team.template.html'
})

export class TeamComponent implements OnInit{
  teams: Team[];
  selectedTeam: Team;
  csv: string;
  enableIndividualCreation: boolean = false;

	constructor(
		private teamService : TeamService,
		private router: Router
  ){}

  ngOnInit(): void {
  	this.retrieveTeams();
  }

  add(csv: string){
    console.log('Let\'s add teams from CSV.');

    this.teamService.add(csv)
      .then(teams => {
        this.teams = teams;
        this.selectedTeam = null;
      });
  }

  overwrite(csv: string){
    console.log('Let\'s overwrite Teams with CSV.');

    this.teamService.overwrite(csv)
      .then(teams => {
        this.teams = teams;
        this.selectedTeam = null;
      });
  }

	retrieveTeams(): void{
		this.teamService.getTeams().then(returnedTeams => this.teams = returnedTeams);
	}
}