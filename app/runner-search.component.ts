import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';
import { Observable }        from 'rxjs/Observable';
import { Subject }           from 'rxjs/Subject';
import { RunnerSearchService } from './runner-search.service';
import { Runner } from './runner';

@Component({
  selector: 'runner-search',
  templateUrl: 'app/runner-search.template.html',
  styleUrls:  ['app/runner-search.component.css'],
  providers: [RunnerSearchService]
})

export class RunnerSearchComponent implements OnInit {
  runners: Observable<Runner[]>;
  private searchTerms = new Subject<string>();

  constructor(
    private runnerSearchService: RunnerSearchService,
    private router: Router) 
  {}
  
  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }
  
  ngOnInit(): void {
    this.runners = this.searchTerms
      .debounceTime(300)        // wait for 300ms pause in events
      .distinctUntilChanged()   // ignore if next search term is same as previous
      .switchMap(term => term   // switch to new observable each time
        // return the http search observable
        ? this.runnerSearchService.search(term)
        // or the observable of empty runners if no search term
        : Observable.of<Runner[]>([]))
      .catch(error => {
        // TODO: real error handling
        console.log(error);
        return Observable.of<Runner[]>([]);
      }
     // this.handleError
      );
  }

  private handleError(error: any): Observable<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Observable.of<Runner[]>([]);
  }

  gotoDetail(runner: Runner): void {
    let link = ['/runners', runner.bib];
    this.router.navigate(link);
  }
}