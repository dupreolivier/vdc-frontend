/* 
 * Copyright (C) 2016 VDC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Component, OnInit } from '@angular/core';

import { Runner } from './runner';
import { Team } from './team';

import { RunService } from './run.service';
import { RunnerService } from './runner.service';
import { TeamService } from './team.service';
import { LoopService } from './loop.service';


@Component({
  selector: 'loop',
  templateUrl: 'app/loop.template.html'
})

export class LoopComponent implements OnInit {
	runners: Runner[];
	teams: Team[];

	constructor(
		private runnerService : RunnerService,
		private teamService : TeamService,
		private loopService : LoopService,
		private runService : RunService
	){}

	ngOnInit(): void {
  	this.retrieveRunners();
  	this.retrieveTeams();
  }

  addLoop(runner: Runner): void {
  	this.loopService.addLoop(runner.bib);
  }

  addTeamLoop(team: Team): void {
  	this.loopService.addLoop(team.bib);
  }

  retrieveRunners(): void{
		this.runnerService.getRunners()
				.then(returnedRunners => this.runners = returnedRunners);
	}
  retrieveTeams(): void{
		this.teamService.getTeams()
				.then(returnedTeams => this.teams = returnedTeams);
	}

	isRunning(): boolean{
		return this.runService.isRunning();
	}
}