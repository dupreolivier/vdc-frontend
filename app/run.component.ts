/* 
 * Copyright (C) 2016 VDC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { AppComponent } from './app.component';
import { RunService } from './run.service';
import { Run } from './run';

import { MomentModule }    from 'angular2-moment';
import * as moment from 'moment';

@Component({
  selector: 'run',
  templateUrl: 'app/run.template.html'
})

export class RunComponent implements OnInit {
  run: Run;
  elapsedTime: number;

	constructor(
		private runService : RunService)
	{
  	moment.locale('fr-fr');	
  }

  ngOnInit(): void {
    this.retrieveRun();
  }

	start(): void {
		this.runService.start(this.run)
				.then(result => {
					this.run = result;
					this.runService.setRun(this.run);
				});
	}

	end(): void {
		this.runService.end(this.run)
				.then(result => {
					this.run = result;
					this.runService.setRun(this.run);
				});
	}

	setLength(newLength: number): void{
		this.run.length = newLength;
	}

	isRunning(): boolean{
		return this.runService.isRunning();
	}

	retrieveRun(): void{
		this.run = this.runService.getRun();

    if(this.run.start){
    	this.runService.elapsedTime().subscribe(ticks => this.elapsedTime = ticks);
    }
	}

	// getDuration(): boolean{
	// 	return this.runService.getDuration();
	// }
}