/* 
 * Copyright (C) 2016 VDC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Injectable }    from '@angular/core';
import { Headers, Http, RequestOptions, RequestOptionsArgs } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Team } from './team';

import { BASE_URL } from './constants';


@Injectable()
export class TeamService {
	teamsUrl = BASE_URL + '/teams';
	private headers = new Headers({
		'Content-Type': 'application/json',
		'Access-Control-Allow-Origin': 'localhost:3000'
	});

  constructor(private http: Http) { }

  getTeams(): Promise<Team[]> {
		console.log('Getting all teams;');
  	console.log('Calling URL: ' + this.teamsUrl);

   	return this.http.get(this.teamsUrl)
						.toPromise()
						.then(response => Promise.resolve(response.json() as Team[]))
						.catch(this.handleError);
	}


	getTeam(bib: string): Promise<Team> {
		console.log('Getting team: ' + bib);

  	const url = `${this.teamsUrl}/${bib}`;
  	console.log('Calling URL: ' + url);

   	return this.http.get(url)
						.toPromise()
						.then(response => Promise.resolve(response.json() as Team))
						.catch(this.handleError);
	}

	add(csv: string): Promise<Team[]> {
		console.log('Adding list of teams from CSV.');
  	const url = `${this.teamsUrl}/add`;
  	
		return this.populateTeams(csv, url);
	}

	overwrite(csv: string): Promise<Team[]> {
		console.log('Overwriting with a list of teams from CSV.');
  	const url = `${this.teamsUrl}/overwrite`;

		return this.populateTeams(csv, url);
	}

	private populateTeams(csv: string, url: string): Promise<Team[]>{
		csv = csv.replace(/\n/g, '$$$$');
  	console.log('Calling URL: ' + url);

		return this.http.post(url, JSON.stringify(csv), {headers: this.headers})
						.toPromise()
						.then(response => Promise.resolve(response.json() as Team[]))
						.catch(this.handleError);
	}

	private handleError(error: any): Promise<any> {
	  console.error('An error occurred', error); // for demo purposes only
	  return Promise.reject(error.message || error);
	}
}