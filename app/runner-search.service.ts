import { Injectable }     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';

import { Runner } from './runner';
import { RunnerService } from './runner.service';

@Injectable()
export class RunnerSearchService {
  constructor(private http: Http,
  						private runnerService: RunnerService)
  {}

  search(term: string): Observable<Runner[]> {
  	const url = `${this.runnerService.runnersUrl}/search?query=${term}`;

    return this.http
               .get(url)
               .map((r: Response) => r.json().data as Runner[]);
  }
}