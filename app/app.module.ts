/* 
 * Copyright (C) 2016 VDC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import './rxjs-extension';

import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { MomentModule }    from 'angular2-moment';
import { Ng2BootstrapModule } from 'ng2-bootstrap/ng2-bootstrap';

import { AppComponent }  from './app.component';
import { RunComponent }  from './run.component';
import { DashboardComponent }  from './dashboard.component';
import { LoopComponent }  from './loop.component';
import { TeamComponent }  from './team.component';
import { RunnerComponent }  from './runner.component';
import { RunnerDetailComponent } from './runner-detail.component';
import { RunnerSearchComponent } from './runner-search.component';

import { RunnerService } from './runner.service';
import { RunService } from './run.service';
import { LoopService } from './loop.service';
import { DashboardService } from './dashboard.service';
import { TeamService } from './team.service';

import { routing } from './app.routing';


@NgModule({
  imports: [ 
  	BrowserModule, 
  	FormsModule,
  	HttpModule,
  	MomentModule,
  	Ng2BootstrapModule,
  	routing ],
  declarations: [ AppComponent, DashboardComponent, RunComponent, TeamComponent, LoopComponent, RunnerDetailComponent, RunnerComponent, RunnerSearchComponent ],
  providers: [RunnerService, LoopService, RunService, DashboardService, TeamService],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
