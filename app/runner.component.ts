/* 
 * Copyright (C) 2016 VDC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Runner } from './runner';
import { RunnerService } from './runner.service';

@Component({
  selector: 'runners',
  templateUrl: 'app/runner.template.html'
})

export class RunnerComponent implements OnInit{
  runners: Runner[];
  selectedRunner: Runner;
  csv: string;
  enableIndividualCreation: boolean = false;

	constructor(
		private runnerService : RunnerService,
		private router: Router
  ){}

  ngOnInit(): void {
  	this.retrieveRunners();
  }

  add(csv: string){
    console.log('Let\'s add Runners from CSV.');

    this.runnerService.add(csv)
      .then(runners => {
        this.runners = runners;
        this.selectedRunner = null;
      });
  }

  overwrite(csv: string){
    console.log('Let\'s overwrite Runners with CSV.');

    this.runnerService.overwrite(csv)
      .then(runners => {
        this.runners = runners;
        this.selectedRunner = null;
      });
  }

  create(bib: string, firstName: string, lastName: string, email: string){
  	// Trim everything...
  	bib = bib.trim();
  	firstName = firstName.trim();
  	lastName = lastName.trim();
  	email = email.trim();

  	// Don't do anything if one argument is missing
	  if (!bib || !firstName || !lastName || !email) { return; }

	  // Else... create the new runner!
  	console.log('Runner ' + bib + ' ' + firstName + ' ' + lastName + ' ' + email);

	  this.runnerService.create(bib, firstName, lastName, email)
	    .then(runner => {
	      this.runners.push(runner);
	      this.selectedRunner = null;
	    });
  }

  delete(runner: Runner): void {
  	console.log('Let\'s delete Runner ' + runner);

		this.runnerService.delete(runner).then(() => {
        this.runners = this.runners.filter(currentRunner => currentRunner !== runner);
        if (this.selectedRunner === runner) { this.selectedRunner = null; }
      });
	}

	gotoDetail(runner: Runner): void { 
		this.selectedRunner = runner;
	  let link = ['/runners', runner.bib];
	  this.router.navigate(link);
	}

	retrieveRunners(): void{
		this.runnerService.getRunners().then(returnedRunners => this.runners = returnedRunners);
	}

  /**
   * @Deprecated since gotoDetail has been added...
   */
	onSelect(runner: Runner): void {
		this.selectedRunner = runner;
	}
}