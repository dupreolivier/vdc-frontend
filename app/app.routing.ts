/* 
 * Copyright (C) 2016 VDC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RunnerComponent }      from './runner.component';
import { RunnerDetailComponent } from './runner-detail.component';
import { DashboardComponent }      from './dashboard.component';
import { RunComponent }      from './run.component';
import { LoopComponent }      from './loop.component';
import { TeamComponent }      from './team.component';

const appRoutes: Routes = [
  {
	  path: '',
	  redirectTo: '/dashboard',
	  pathMatch: 'full'
	},
	{
    path: 'dashboard',
    component: DashboardComponent
  },
	{
    path: 'runners',
    component: RunnerComponent
  },
  {
    path: 'run',
    component: RunComponent
  },
  {
    path: 'teams',
    component: TeamComponent
  },
  {
    path: 'loops',
    component: LoopComponent
  },
  {
    path: 'runners/:bib',
    component: RunnerDetailComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
