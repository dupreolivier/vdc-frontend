/* 
 * Copyright (C) 2016 VDC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Injectable }    	from '@angular/core';
import { Headers, Http } 	from '@angular/http';
import { Observable } 		from 'rxjs/Rx';

import 'rxjs/add/operator/toPromise';

import { Run } from './run';

import { BASE_URL, RUN_NAME, LENGTH } from './constants';

@Injectable()
export class RunService {
	private run: Run = {
		title: RUN_NAME,
		date: '25 Septembre 2016',
		location: 'Toulouse',
		length: LENGTH,
		start: null,
		end: null
  };
  private timer : Observable<number>;

	private runsUrl = BASE_URL + '/runs';	
	private headers = new Headers({
		'Content-Type': 'application/json',
		'Access-Control-Allow-Origin': 'localhost:3000'
	});

	private running: boolean = false;

  constructor(private http: Http) { }

  start(run: Run): Promise<Run> {
		console.log('Starting the run ' + run.title);
  	const url = `${this.runsUrl}/start`;
  	console.log('Calling URL: ' + url);

  	this.running = true;

  	if(!this.timer){
  		console.log("Reset timer");
			this.timer = Observable.timer(0, 1000);
  	}

   	return this.http.post(url, JSON.stringify(run), {headers: this.headers})
						.toPromise()
						.then(response => {
							var result = Promise.resolve(response.json() as Run);
							return result
						})
						.catch(this.handleError);
	}
	
  end(run: Run): Promise<Run> {
		console.log('Ending the run ' + run.title);
  	const url = `${this.runsUrl}/end`;
  	console.log('Calling URL: ' + url);

  	this.running = false;

   	return this.http.put(url, JSON.stringify(run), {headers: this.headers})
						.toPromise()
						.then(response => {
							return Promise.resolve(response.json() as Run)
						})
						.catch(this.handleError);
	}

	getRun(): Run{
		return this.run;
	}

	setRun(run: Run): void{
		this.run = run;
	}

	elapsedTime(): Observable<number>{
    	// console.log("Timer");
    	// console.log(new Date(this.run.start));
	    // let timer = Observable.timer(new Date(this.run.start),1000);
	    // timer.subscribe(t=>this.ticks = t);
	    return this.timer;
	}

	// getDuration(): Promise<string> {
	// 	console.log('Duration of the run...');
 //  	const url = `${this.runsUrl}/duration`;
 //  	console.log('Calling URL: ' + url);

 //  	this.running = false;

 //   	return this.http.get(url, {headers: this.headers})
	// 					.toPromise()
	// 					.then(response => Promise.resolve(response.text()))
	// 					.catch(this.handleError);
	// }

	setLength(length: number): Promise<any> {
		console.log('Setting run\'s length to: ' + length);

  	const url = `${this.runsUrl}/length/${length}`;
  	console.log('Calling URL: ' + url);

		return this.http.put(url, {headers: this.headers})
				    .toPromise()
				    .then(() => Promise.resolve(true))
				    .catch(this.handleError);
	}

	isRunning(): boolean{
		return this.running;
	}

	private handleError(error: any): Promise<any> {
	  console.error('An error occurred', error);
	  return Promise.reject(error.message || error);
	}
}